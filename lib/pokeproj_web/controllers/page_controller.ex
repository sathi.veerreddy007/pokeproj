defmodule PokeprojWeb.PageController do
  use PokeprojWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
