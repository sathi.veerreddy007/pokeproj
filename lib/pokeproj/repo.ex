defmodule Pokeproj.Repo do
  use Ecto.Repo,
    otp_app: :pokeproj,
    adapter: Ecto.Adapters.Postgres
end
